# map.zkav.ch

Since we would like to operate [zkav.ch](https://zkav.ch/) as privacy-
friendly as possible, we use a proxy to the OpenStreetMap tile server.
This proxy is hosted on a server in Switzerland and is operated by us.
This way, we can ensure that no IP address is sent to OpenStreetMap.
